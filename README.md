# Linkanything

Provides news stream on anything.

## Link list

```json
{
  "Title": "Domain Driven Design to the rescue of legacy code",
  "Description": "Talk über die Anwendung von DDD für die Modernisierung von Legacy Systemen",
  "Type": "Video",
  "Created": "2023-03-03"
  "URI: https://www.youtube.com/watch?v=1AlCTVXs5KE"
  "Tags": "legacy, DDD",
  "Recommendation": "Lohnt sich"
}
```
