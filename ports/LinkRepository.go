package ports

import "gitlab.com/codekeepers/linkanything/domain"

type LinkRepository interface {
	Create(link domain.Link) domain.Link
}
